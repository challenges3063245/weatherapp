//
//  UserViewData.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 08/02/24.
//

import Foundation

struct UserViewData {

    private let user: User

    var hi: String {
        "Hi " + (user.name ?? "") + "!"
    }

    var welcome: String {
        "Welcome " + (user.name ?? "") + "!"
    }

    var name: String {
        user.name ?? ""
    }

    var email: String {
        user.email ?? ""
    }

    var phone: String {
        user.phone ?? ""
    }

    init(user: User) {
        self.user = user
    }

}
