//
//  CustomTextView.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 07/02/24.
//

import SwiftUI

struct CustomTextView: View {

    @Binding var text: String

    var body: some View {
        TextField(text: $text) {
            Text("Name")
        }
        .frame(height: 50)
        .padding(.horizontal)

        Rectangle()
            .fill(Color.accentColor)
            .frame(height: 1)
            .padding(.top, -10)
            .padding(.horizontal)
    }

}
