//
//  CoreDataManager.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 06/02/24.
//

import Foundation
import CoreData

protocol CoreDataManagerProtocol {
    var persistentContainer: NSPersistentContainer { get set }
    var context: NSManagedObjectContext { get }
    var queue: DispatchQueue { get }
    func saveContext ()
}

class CoreDataManager: CoreDataManagerProtocol {
    static var shared: CoreDataManager = CoreDataManager()

    private init() {
        self.context.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
    }

    var queue = DispatchQueue(
        label: "WeatherApp.CoreData.Manager.DataSource",
        qos: .userInteractive,
        attributes: .concurrent,
        autoreleaseFrequency: .inherit,
        target: .main
    )

    // MARK: - Core Data stack
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "DataModel")
        container.loadPersistentStores(completionHandler: { (/*storeDescription*/_, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support
    var context: NSManagedObjectContext {
        return persistentContainer.viewContext
    }

    private func block(completion: @escaping () -> Void) {
        self.queue.async(group: nil, qos: .userInteractive, flags: .barrier, execute: completion)
    }

    func saveContext () {
        if self.context.hasChanges {
            try? self.context.save()
        }
    }

}
