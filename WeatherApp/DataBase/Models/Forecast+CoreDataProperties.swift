//
//  Forecast+CoreDataProperties.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 07/02/24.
//
//

import Foundation
import CoreData


extension Forecast {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Forecast> {
        return NSFetchRequest<Forecast>(entityName: "Forecast")
    }

    @NSManaged public var forecastDay: NSSet?

}

// MARK: Generated accessors for forecastDay
extension Forecast {

    @objc(addForecastDayObject:)
    @NSManaged public func addToForecastDay(_ value: ForecastDayData)

    @objc(removeForecastDayObject:)
    @NSManaged public func removeFromForecastDay(_ value: ForecastDayData)

    @objc(addForecastDay:)
    @NSManaged public func addToForecastDay(_ values: NSSet)

    @objc(removeForecastDay:)
    @NSManaged public func removeFromForecastDay(_ values: NSSet)

}

extension Forecast : Identifiable {

}
