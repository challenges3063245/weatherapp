//
//  Astro+CoreDataProperties.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 07/02/24.
//
//

import Foundation
import CoreData


extension Astro {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Astro> {
        return NSFetchRequest<Astro>(entityName: "Astro")
    }

    @NSManaged public var sunrise: String?
    @NSManaged public var sunset: String?
    @NSManaged public var moonrise: String?
    @NSManaged public var moonset: String?
    @NSManaged public var isMoonUp: Int16
    @NSManaged public var isSunUp: Int16
    @NSManaged public var forecastDayData: ForecastDayData?

}

extension Astro : Identifiable {

}
