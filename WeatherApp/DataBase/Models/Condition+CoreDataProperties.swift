//
//  Condition+CoreDataProperties.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 06/02/24.
//
//

import Foundation
import CoreData


extension Condition {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Condition> {
        return NSFetchRequest<Condition>(entityName: "Condition")
    }

    @NSManaged public var code: Int64
    @NSManaged public var icon: String?
    @NSManaged public var text: String?
    @NSManaged public var weather: Weather?

}

extension Condition : Identifiable {

}
