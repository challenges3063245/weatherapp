//
//  Forecast+CoreDataClass.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 07/02/24.
//
//

import Foundation
import CoreData

@objc(Forecast)
public class Forecast: NSManagedObject {

    required public convenience init(from decoder: Decoder) throws {
        let context = CoreDataManager.shared.context
        guard let entity = NSEntityDescription.entity(forEntityName: "Forecast", in: context) else {
                fatalError("Failed to decode Forecast")
        }
        self.init(entity: entity, insertInto: context)

        let container = try decoder.container(keyedBy: CodingKeys.self)

        let array = try container.decodeIfPresent([ForecastDayData].self, forKey: .forecastday) ?? []
        self.forecastDay = NSSet(array: array)
    }

    var forecastDays: [ForecastDayData] {
        self.forecastDay?.allObjects as? [ForecastDayData] ?? []
    }

}

extension Forecast: Decodable {

    enum CodingKeys: String, CodingKey {
        case forecastday
    }

}
