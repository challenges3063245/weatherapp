//
//  ForecastDayData+CoreDataProperties.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 07/02/24.
//
//

import Foundation
import CoreData


extension ForecastDayData {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ForecastDayData> {
        return NSFetchRequest<ForecastDayData>(entityName: "ForecastDayData")
    }

    @NSManaged public var date: String?
    @NSManaged public var astro: Astro?
    @NSManaged public var day: Day?
    @NSManaged public var forecast: Forecast?

}

extension ForecastDayData : Identifiable {

}
