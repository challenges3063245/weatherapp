//
//  Sesion+CoreDataProperties.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 06/02/24.
//
//

import Foundation
import CoreData


extension Sesion {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Sesion> {
        return NSFetchRequest<Sesion>(entityName: "Sesion")
    }

    @NSManaged public var isLogged: Bool
    @NSManaged public var loginDate: Date?
    @NSManaged public var biometrics: Bool
    @NSManaged public var user: User?

}

extension Sesion : Identifiable {

}
