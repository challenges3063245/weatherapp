//
//  User+CoreDataProperties.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 06/02/24.
//
//

import Foundation
import CoreData


extension User {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        let fetchRequest = NSFetchRequest<User>(entityName: "User")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "name", ascending: false)]
        return fetchRequest
    }

    @NSManaged public var email: String?
    @NSManaged public var pass: String?
    @NSManaged public var imageData: Data?
    @NSManaged public var name: String?
    @NSManaged public var phone: String?
    @NSManaged public var firstLoginDidPass: Bool
    @NSManaged public var sesion: Sesion?

}

extension User : Identifiable {

}
