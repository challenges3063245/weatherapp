//
//  Location+CoreDataProperties.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 06/02/24.
//
//

import Foundation
import CoreData


extension Location {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Location> {
        return NSFetchRequest<Location>(entityName: "Location")
    }

    @NSManaged public var country: String?
    @NSManaged public var lat: Double
    @NSManaged public var lon: Double
    @NSManaged public var localtime: String?
    @NSManaged public var epoch: Double
    @NSManaged public var name: String?
    @NSManaged public var region: String?
    @NSManaged public var tzId: String?

}

extension Location : Identifiable {

}
