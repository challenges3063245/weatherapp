//
//  WeatherData+CoreDataClass.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 06/02/24.
//
//

import Foundation
import CoreData

@objc(WeatherData)
public class WeatherData: NSManagedObject {

    required public convenience init(from decoder: Decoder) throws {
        let context = CoreDataManager.shared.context
        guard let entity = NSEntityDescription.entity(forEntityName: "WeatherData", in: context) else {
            fatalError("Failed to decode WeatherData")
        }
        self.init(entity: entity, insertInto: context)

        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.date = Date()
        self.current = try container.decodeIfPresent(Weather.self, forKey: .current)
        self.location = try container.decodeIfPresent(Location.self, forKey: .location)
        self.forecast = try container.decodeIfPresent(Forecast.self, forKey: .forecast)
        self.code = self.current?.condition?.codeString ?? ""
    }

}

extension WeatherData: Decodable {

    enum CodingKeys: String, CodingKey {
        case current
        case location
        case forecast
    }

}
