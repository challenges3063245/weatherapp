//
//  ForecastDayData+CoreDataClass.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 07/02/24.
//
//

import Foundation
import CoreData

@objc(ForecastDayData)
public class ForecastDayData: NSManagedObject {

    required public convenience init(from decoder: Decoder) throws {
        let context = CoreDataManager.shared.context
        guard let entity = NSEntityDescription.entity(forEntityName: "ForecastDayData", in: context) else {
                fatalError("Failed to decode ForecastDayData")
        }
        self.init(entity: entity, insertInto: context)

        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.date = try container.decodeIfPresent(String.self, forKey: .date)
        self.astro = try container.decodeIfPresent(Astro.self, forKey: .astro)
        self.day = try container.decodeIfPresent(Day.self, forKey: .day)
    }

}

extension ForecastDayData: Decodable {

    enum CodingKeys: String, CodingKey {
        case date
        case astro
        case day
    }

}
