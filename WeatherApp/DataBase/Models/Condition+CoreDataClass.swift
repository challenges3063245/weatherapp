//
//  Condition+CoreDataClass.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 06/02/24.
//
//

import Foundation
import CoreData

@objc(Condition)
public class Condition: NSManagedObject {

    required public convenience init(from decoder: Decoder) throws {
        let context = CoreDataManager.shared.context
        guard let entity = NSEntityDescription.entity(forEntityName: "Condition", in: context) else {
                fatalError("Failed to decode Condition")
        }
        self.init(entity: entity, insertInto: context)

        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.code = try container.decodeIfPresent(Int64.self, forKey: .code) ?? 0
        self.icon = try container.decodeIfPresent(String.self, forKey: .icon)
        self.text = try container.decodeIfPresent(String.self, forKey: .text)
    }

    var codeString: String {
        "\(code)-\(text ?? "")-\(icon ?? "")"
    }

}

extension Condition: Decodable {

    enum CodingKeys: String, CodingKey {
        case code
        case icon
        case text
    }

}
