//
//  User+CoreDataClass.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 06/02/24.
//
//

import Foundation
import CoreData

@objc(User)
public class User: NSManagedObject {

    public convenience init(email: String? = nil, imageData: Data? = nil, name: String?, pass: String?, phone: String? = nil, firstLoginDidPass: Bool, sesion: Sesion?) {
        let context = CoreDataManager.shared.context
        guard let entity = NSEntityDescription.entity(forEntityName: "User", in: context) else {
                fatalError("Failed to decode User")
        }
        self.init(entity: entity, insertInto: context)

        self.email = email
        self.imageData = imageData
        self.name = name
        self.pass = pass
        self.phone = phone
        self.firstLoginDidPass = firstLoginDidPass
        self.sesion = sesion
    }

}
