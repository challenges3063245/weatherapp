//
//  Sesion+CoreDataClass.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 06/02/24.
//
//

import Foundation
import CoreData

@objc(Sesion)
public class Sesion: NSManagedObject {

    public convenience init(isLogged: Bool, loginDate: Date?, biometrics: Bool) {
        let context = CoreDataManager.shared.context
        guard let entity = NSEntityDescription.entity(forEntityName: "Sesion", in: context) else {
                fatalError("Failed to decode Sesion")
        }
        self.init(entity: entity, insertInto: context)

        self.isLogged = isLogged
        self.loginDate = loginDate
        self.biometrics = biometrics
    }

}
