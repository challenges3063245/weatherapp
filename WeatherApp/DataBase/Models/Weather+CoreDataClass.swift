//
//  Weather+CoreDataClass.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 06/02/24.
//
//

import Foundation
import CoreData

@objc(Weather)
public class Weather: NSManagedObject {

    required public convenience init(from decoder: Decoder) throws {
        let context = CoreDataManager.shared.context
        guard let entity = NSEntityDescription.entity(forEntityName: "Weather", in: context) else {
            fatalError("Failed to decode Weather")
        }
        self.init(entity: entity, insertInto: context)

        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.cloud = try container.decodeIfPresent(Int16.self, forKey: .cloud) ?? 0
        self.condition = try container.decodeIfPresent(Condition.self, forKey: .condition)
        self.feelslikeC = try container.decodeIfPresent(Double.self, forKey: .feelsLikeC) ?? 0
        self.feelslikeF = try container.decodeIfPresent(Double.self, forKey: .feelsLikeF) ?? 0
        self.gustKPH = try container.decodeIfPresent(Double.self, forKey: .gustKPM) ?? 0
        self.gustMPH = try container.decodeIfPresent(Double.self, forKey: .gustMPM) ?? 0
        self.humidity = try container.decodeIfPresent(Double.self, forKey: .humidity) ?? 0
        self.isDay = try container.decodeIfPresent(Int16.self, forKey: .isDay) ?? 0
        self.lastUpdated = try container.decodeIfPresent(String.self, forKey: .lastUpdated)
        self.lastUpdatedEpoch = try container.decodeIfPresent(Double.self, forKey: .lastUpdatedEpoch) ?? 0
        self.precipin = try container.decodeIfPresent(Double.self, forKey: .precipin) ?? 0
        self.precipmm = try container.decodeIfPresent(Double.self, forKey: .precipmm) ?? 0
        self.pressurein = try container.decodeIfPresent(Double.self, forKey: .pressurein) ?? 0
        self.pressuremb = try container.decodeIfPresent(Double.self, forKey: .pressuremb) ?? 0
        self.tempC = try container.decodeIfPresent(Double.self, forKey: .tempC) ?? 0
        self.tempF = try container.decodeIfPresent(Double.self, forKey: .tempF) ?? 0
        self.uv = try container.decodeIfPresent(Double.self, forKey: .uv) ?? 0
        self.viskm = try container.decodeIfPresent(Double.self, forKey: .viskm) ?? 0
        self.vismiles = try container.decodeIfPresent(Double.self, forKey: .vismiles) ?? 0
        self.windDegree = try container.decodeIfPresent(Double.self, forKey: .windDegree) ?? 0
        self.winddir = try container.decodeIfPresent(String.self, forKey: .windDir)
        self.windkph = try container.decodeIfPresent(Double.self, forKey: .windkph) ?? 0
        self.windmph = try container.decodeIfPresent(Double.self, forKey: .windmph) ?? 0
    }

}

extension Weather: Decodable {

    enum CodingKeys: String, CodingKey {
        case cloud
        case condition
        case feelsLikeC = "feelslike_c"
        case feelsLikeF = "feelslike_f"
        case gustKPM = "gust_kph"
        case gustMPM = "gust_mph"
        case humidity
        case isDay = "is_day"
        case lastUpdated = "last_updated"
        case lastUpdatedEpoch = "last_updated_epoch"
        case precipin = "precip_in"
        case precipmm = "precip_mm"
        case pressurein = "pressure_in"
        case pressuremb = "pressure_mb"
        case tempC = "temp_c"
        case tempF = "temp_f"
        case uv
        case viskm = "vis_km"
        case vismiles = "vis_miles"
        case windDegree = "wind_degree"
        case windDir = "wind_dir"
        case windkph = "wind_kph"
        case windmph = "wind_mph"
    }

}
