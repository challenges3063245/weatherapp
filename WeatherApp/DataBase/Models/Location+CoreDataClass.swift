//
//  Location+CoreDataClass.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 06/02/24.
//
//

import Foundation
import CoreData

@objc(Location)
public class Location: NSManagedObject {

    required public convenience init(from decoder: Decoder) throws {
        let context = CoreDataManager.shared.context
        guard let entity = NSEntityDescription.entity(forEntityName: "Location", in: context) else {
                fatalError("Failed to decode Location")
        }
        self.init(entity: entity, insertInto: context)

        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.country = try container.decodeIfPresent(String.self, forKey: .country)
        self.lat = try container.decodeIfPresent(Double.self, forKey: .lat) ?? 0
        self.lon = try container.decodeIfPresent(Double.self, forKey: .lon) ?? 0
        self.localtime = try container.decodeIfPresent(String.self, forKey: .localtime)
        self.epoch = try container.decodeIfPresent(Double.self, forKey: .localtimeEpoch) ?? 0
        self.name = try container.decodeIfPresent(String.self, forKey: .name)
        self.region = try container.decodeIfPresent(String.self, forKey: .region)
        self.tzId = try container.decodeIfPresent(String.self, forKey: .tzId)
    }

}

extension Location: Decodable {

    enum CodingKeys: String, CodingKey {
        case country
        case lat
        case lon
        case localtime
        case localtimeEpoch = "localtime_epoch"
        case name
        case region
        case tzId = "tz_id"
    }

}
