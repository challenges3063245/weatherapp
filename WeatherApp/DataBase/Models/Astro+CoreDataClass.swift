//
//  Astro+CoreDataClass.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 07/02/24.
//
//

import Foundation
import CoreData

@objc(Astro)
public class Astro: NSManagedObject {

    required public convenience init(from decoder: Decoder) throws {
        let context = CoreDataManager.shared.context
        guard let entity = NSEntityDescription.entity(forEntityName: "Astro", in: context) else {
                fatalError("Failed to decode Astro")
        }
        self.init(entity: entity, insertInto: context)

        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.sunrise = try container.decodeIfPresent(String.self, forKey: .sunrise)
        self.sunset = try container.decodeIfPresent(String.self, forKey: .sunset)
        self.moonrise = try container.decodeIfPresent(String.self, forKey: .moonrise)
        self.moonset = try container.decodeIfPresent(String.self, forKey: .moonset)
        self.isMoonUp = try container.decodeIfPresent(Int16.self, forKey: .isMoonUp) ?? 0
        self.isSunUp = try container.decodeIfPresent(Int16.self, forKey: .isSunUp) ?? 0
    }

}

extension Astro: Decodable {

    enum CodingKeys: String, CodingKey {
        case sunrise
        case sunset
        case moonrise
        case moonset
        case isMoonUp = "is_moon_up"
        case isSunUp = "is_sun_up"
        
    }

}
