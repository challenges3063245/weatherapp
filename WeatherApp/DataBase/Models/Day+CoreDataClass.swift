//
//  Day+CoreDataClass.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 07/02/24.
//
//

import Foundation
import CoreData

@objc(Day)
public class Day: NSManagedObject {

    required public convenience init(from decoder: Decoder) throws {
        let context = CoreDataManager.shared.context
        guard let entity = NSEntityDescription.entity(forEntityName: "Day", in: context) else {
                fatalError("Failed to decode Day")
        }
        self.init(entity: entity, insertInto: context)

        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.maxTempC = try container.decodeIfPresent(Double.self, forKey: .maxTempC) ?? 0
        self.maxTempF = try container.decodeIfPresent(Double.self, forKey: .maxTempF) ?? 0
        self.minTempC = try container.decodeIfPresent(Double.self, forKey: .minTempC) ?? 0
        self.minTempF = try container.decodeIfPresent(Double.self, forKey: .minTempF) ?? 0
        self.avgTempC = try container.decodeIfPresent(Double.self, forKey: .avgTempC) ?? 0
        self.avgTempF = try container.decodeIfPresent(Double.self, forKey: .avgTempF) ?? 0
    }

}

extension Day: Decodable {

    enum CodingKeys: String, CodingKey {
        case maxTempC = "maxtemp_c"
        case maxTempF = "maxtemp_f"
        case minTempC = "mintemp_c"
        case minTempF = "mintemp_f"
        case avgTempC = "avgtemp_c"
        case avgTempF = "avgtemp_f"
    }

}
