//
//  WeatherData+CoreDataProperties.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 06/02/24.
//
//

import Foundation
import CoreData


extension WeatherData {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<WeatherData> {
        return NSFetchRequest<WeatherData>(entityName: "WeatherData")
    }

    @NSManaged public var date: Date?
    @NSManaged public var code: String?
    @NSManaged public var current: Weather?
    @NSManaged public var location: Location?
    @NSManaged public var forecast: Forecast?

}

extension WeatherData : Identifiable {

}
