//
//  Weather+CoreDataProperties.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 06/02/24.
//
//

import Foundation
import CoreData


extension Weather {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Weather> {
        return NSFetchRequest<Weather>(entityName: "Weather")
    }

    @NSManaged public var cloud: Int16
    @NSManaged public var feelslikeC: Double
    @NSManaged public var feelslikeF: Double
    @NSManaged public var gustKPH: Double
    @NSManaged public var gustMPH: Double
    @NSManaged public var humidity: Double
    @NSManaged public var isDay: Int16
    @NSManaged public var lastUpdated: String?
    @NSManaged public var lastUpdatedEpoch: Double
    @NSManaged public var precipin: Double
    @NSManaged public var precipmm: Double
    @NSManaged public var pressurein: Double
    @NSManaged public var pressuremb: Double
    @NSManaged public var tempC: Double
    @NSManaged public var tempF: Double
    @NSManaged public var uv: Double
    @NSManaged public var viskm: Double
    @NSManaged public var vismiles: Double
    @NSManaged public var windDegree: Double
    @NSManaged public var winddir: String?
    @NSManaged public var windkph: Double
    @NSManaged public var windmph: Double
    @NSManaged public var condition: Condition?

}

extension Weather : Identifiable {

}
