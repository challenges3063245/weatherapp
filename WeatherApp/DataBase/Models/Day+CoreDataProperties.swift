//
//  Day+CoreDataProperties.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 07/02/24.
//
//

import Foundation
import CoreData


extension Day {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Day> {
        return NSFetchRequest<Day>(entityName: "Day")
    }

    @NSManaged public var maxTempC: Double
    @NSManaged public var maxTempF: Double
    @NSManaged public var minTempC: Double
    @NSManaged public var minTempF: Double
    @NSManaged public var avgTempC: Double
    @NSManaged public var avgTempF: Double
    @NSManaged public var forecastDayData: ForecastDayData?

}

extension Day : Identifiable {

}
