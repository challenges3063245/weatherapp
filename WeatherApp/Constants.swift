//
//  Constants.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 07/02/24.
//

import Foundation

struct Constant {

    static var BIOMETRICS: String = "biometrics"
    static var PREVIOUSLY_LOGGED: String = "previouslyLogged"
    static var PREVIOUSLY_LOGGEDID: String = "previouslyLoggedID"

}
