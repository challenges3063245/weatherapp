//
//  ImpactFeedback.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 07/02/24.
//

import UIKit

class FeedbackGenerator {
    static let generator = UIImpactFeedbackGenerator(style: .light)
}
