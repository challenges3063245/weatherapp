//
//  UserDefaults.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 08/02/24.
//

import Foundation

class Preferences {

    static var biometrics: Bool {
        get { UserDefaults.standard.bool(forKey: Constant.BIOMETRICS) }
        set {
            UserDefaults.standard.setValue(newValue, forKey: Constant.BIOMETRICS)
        }
    }

    static var previouslyLogged: Bool {
        get { UserDefaults.standard.bool(forKey: Constant.PREVIOUSLY_LOGGED) }
        set {
            UserDefaults.standard.setValue(newValue, forKey: Constant.PREVIOUSLY_LOGGED)
        }
    }

    static var previouslyLoggedID: Bool {
        get { UserDefaults.standard.bool(forKey: Constant.PREVIOUSLY_LOGGEDID) }
        set {
            UserDefaults.standard.setValue(newValue, forKey: Constant.PREVIOUSLY_LOGGEDID)
        }
    }

}
