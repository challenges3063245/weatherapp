//
//  FormViewModel.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 07/02/24.
//

import Foundation
import SwiftUI

struct FormViewModel {
    var actionName: String
    var inputViewModels: [InputFieldViewModel]

    init(actionName: String, inputViewModels: [InputFieldViewModel] = []) {
        self.actionName = actionName
        self.inputViewModels = inputViewModels
    }

}

struct InputFieldViewModel: Identifiable {
    var id: UUID = UUID()
    var fieldType: FieldType = .textField
    var placeholder: String = "Placeholder"
    var imageName: String = "eye"
    var text: TextStore
    var keyboardType: Int
    var showPassword: Bool

    init(fieldType: FieldType, placeholder: String, keyboardType: Int = 0) {
        self.fieldType = fieldType
        self.placeholder = placeholder
        self.keyboardType = keyboardType
        self.showPassword = fieldType != .secureField
        self.text = TextStore()
    }

    mutating func togglePasswordTextField() {
        self.showPassword.toggle()
        self.imageName = showPassword ? "eye.slash" : "eye"
    }
}

class TextStore {
    @Published var text: String = ""
    @Published var showFeedback: Bool = false
    @Published var _showFeedBack: (() -> Bool)?
}

enum FieldType {
    case textField, secureField, email
}
