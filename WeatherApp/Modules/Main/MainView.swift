//
//  MainView.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 07/02/24.
//

import SwiftUI
import LocalAuthentication

struct MainView: View {

    @Namespace private var animation

    @State private var animate = false

    @ObservedObject var viewModel: MainViewModel
    @AppStorage(Constant.BIOMETRICS) private var biometrics: Bool = false
    @AppStorage(Constant.PREVIOUSLY_LOGGED) private var previouslyLogged: Bool = false
    @AppStorage(Constant.PREVIOUSLY_LOGGEDID) private var previouslyLoggedID: Bool = false

    init(viewModel: MainViewModel) {
        self.viewModel = viewModel
    }

    var body: some View {
        ZStack {
            VStack {
                Group {
                    Text("Weather App!")
                        .font(.title)
                        .bold()
                        .padding(EdgeInsets(top: 60, leading: 0, bottom: 30, trailing: 0))
                        .foregroundStyle(.gray)

                    forms

                    Button(action: action) {
                        actionButton
                    }
                    .fullScreenCover(isPresented: $viewModel.isLogged) {
                        Home(isLogged: $viewModel.isLogged)
                    }
                    .padding(.bottom, 20)

                    if !viewModel.isFlipped {
                        toggle
                    }
                }
                .rotation3DEffect(.degrees(viewModel.rotationDegrees), axis: (0, 1, 0))
            }
            .overlay(alignment: .topTrailing) {
                Button(action: flipView) {
                    Text(viewModel.isFlipped ? "Login" : "Sign Up")
                }
                .padding(EdgeInsets(top: 20, leading: 20, bottom: 0, trailing: 20))
                .rotation3DEffect(.degrees(viewModel.rotationDegrees), axis: (0, 1, 0))
            }
            .background {
                RoundedRectangle(cornerRadius: 15)
                    .fill(.background)
                    .shadow(radius: 4)
            }
            .padding(.horizontal, 30)
            .rotation3DEffect(.degrees(viewModel.rotationDegrees), axis: (0, 1, 0))
            .animation(.spring(dampingFraction: 0.7), value: viewModel.isFlipped)
            .animation(.spring(duration: 0.25), value: biometrics)
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .ignoresSafeArea()
        .background {
            Color.customBackground
                .ignoresSafeArea()
        }
        .onAppear {
            viewModel.checkSesion()
        }
    }

    @ViewBuilder
    private var toggle: some View {
        if viewModel.isUsingID {
            Button(action: usePassword) {
                Text("Use Password")
                    .tint(.gray)
                    .underline()
                    .padding(.bottom)
                    .font(.system(size: 12))
            }
            .padding(.bottom, 30)
        } else {
            Toggle(isOn: $biometrics.animation()) {
                Text("biometrics")
                    .frame(maxWidth: .infinity, alignment: .trailing)
                    .foregroundStyle(.gray)
            }
            .onChange(of: biometrics) {
                viewModel.setIdType()
            }
            .tint(.accentColor)
            .padding(.bottom, 20)
            .padding(.trailing, 20)
        }
    }

    @ViewBuilder
    private var forms: some View {
        ForEach(0..<viewModel.viewFlags.count, id: \.self) {
            if viewModel.viewFlags[$0] {
                FormView(viewModel: viewModel.formViewModels[$0])
                    .animation(.spring(duration: 0.25), value: biometrics)

                if FormType(rawValue: $0) == .login {
                    Button(action: recovering) {
                        Text("Recover password")
                            .tint(.gray)
                            .underline()
                            .font(.system(size: 12))
                    }
                    .padding()
                } else {
                    Spacer()
                        .frame(height: 50)
                }
            }
        }
    }

    @ViewBuilder
    private var actionButton: some View {
        let condition: Bool = biometrics && !viewModel.isFlipped
        ZStack(alignment: .center) {
            ZStack {
                Circle().fill(.secondary)
                    .frame(width: condition ? 80 : 0, height: condition ? 80 : 0)
                Image(systemName: "faceid")
                    .resizable()
                    .symbolEffect(.bounce.down, value: animate)
                    .frame(width: condition ? 40 : 0, height: condition ? 40 : 0)
            }
            let text: String = viewModel.isRecovering ? "Set up" : !viewModel.isFlipped ? "Login" : "Sign up"
            Text(text)
                .padding(condition ? 0 : 10)
                .frame(width: condition ? 0 : 200, height: condition ? 0 : 40)
                .background {
                    RoundedRectangle(cornerRadius: 10)
                        .stroke(Color.accentColor, lineWidth: condition ? 0 : 2)
                }
        }
    }

    private func action() {
        if biometrics && (viewModel.isLogging || viewModel.isUsingID) {
            animate.toggle()
            viewModel.authenticateWithBiometrics()
        } else {
            viewModel.validateAction()
        }
    }

    private func flipView() {
        if self.viewModel.isFlipped {
            self.viewModel.formType = biometrics && previouslyLoggedID ? .biometric : .login
        } else {
            self.viewModel.formType = .signup
        }
        self.hideKeyboard()
    }

    private func usePassword() {
        self.biometrics = false
        self.previouslyLoggedID = false
        self.viewModel.setIdType()
    }

    private func recovering() {
        self.viewModel.formType = .recovering
    }

    private func hideKeyboard() {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }

}
