//
//  MainRepository.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 08/02/24.
//

import Foundation
import Combine

class MainRepository {

    let dbClient: DataSource

    init(dbClient: DataSource) {
        self.dbClient = dbClient
    }

    var userData: AnyPublisher<[User], Error> {
        return dbClient.fetchObjects().eraseToAnyPublisher()
    }

}
