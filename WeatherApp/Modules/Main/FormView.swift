//
//  FormView.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 07/02/24.
//

import SwiftUI

struct FormView: View {

    @State var viewModel: FormViewModel

    var body: some View {
        VStack(spacing: 16) {
            ForEach(0..<viewModel.inputViewModels.count, id: \.self) { i in
                let text = $viewModel.inputViewModels[i].text.text
                let textPublisher = viewModel.inputViewModels[i].text.$text
                let placeholder = viewModel.inputViewModels[i].placeholder
                Group {
                    if viewModel.inputViewModels[i].showPassword {
                        FocusedTextField(text: text, placeholder: placeholder, fieldType: .textField) { isFocused in
                            let condition = isFocused ? false : viewModel.inputViewModels[i].text._showFeedBack?() ?? false
                            viewModel.inputViewModels[i].text.showFeedback = condition
                        }
                    } else {
                        FocusedTextField(text: text, placeholder: placeholder, fieldType: .secureField) { isFocused in
                            let condition = isFocused ? false : viewModel.inputViewModels[i].text._showFeedBack?() ?? false
                            viewModel.inputViewModels[i].text.showFeedback = condition
                        }
                    }
                }
                .onReceive(textPublisher) { value in
                    let condition: Bool = viewModel.inputViewModels[i].text._showFeedBack?() ?? false
                    viewModel.inputViewModels[i].text.showFeedback = condition
                }
                .keyboardType(.init(rawValue: viewModel.inputViewModels[i].keyboardType) ?? .default)
                .multilineTextAlignment(.center)
                .frame(height: 50)
                .overlay(alignment: .bottom) {
                    let showFeedBack: Bool = viewModel.inputViewModels[i].text.showFeedback
                    if showFeedBack {
                        Color.red
                            .frame(height: 1)
                    } else {
                        Color.accentColor
                            .frame(height: 1)
                    }
                }
                .overlay(alignment: .trailing) {
                    if viewModel.inputViewModels[i].fieldType == .secureField {
                        Button {
                            viewModel.inputViewModels[i].togglePasswordTextField()
                        } label: {
                            Image(systemName: viewModel.inputViewModels[i].imageName)
                        }
                        .padding(.trailing)
                    }
                }
                .padding(.horizontal)
            }
        }
        .frame(maxWidth: .infinity)
    }

}
