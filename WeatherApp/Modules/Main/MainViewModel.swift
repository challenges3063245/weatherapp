//
//  MainViewModel.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 07/02/24.
//

import Foundation
import Combine
import LocalAuthentication

enum FormType: Int {
    case login, signup, recovering, biometric
}

class MainViewModel: ObservableObject {

    @Published var isLogged: Bool = false
    @Published var isFlipped: Bool
    @Published var viewFlags: [Bool]

    let useCase: MainUseCase
    var subscriptions = Set<AnyCancellable>()

    var canFlip: Bool = true

    init(useCase: MainUseCase) {
        let initialValue: FormType = Preferences.previouslyLoggedID ? .biometric : Preferences.previouslyLogged ? .login : .signup
        self.useCase = useCase
        self.formType = initialValue
        self.viewFlags = (0..<formViewModels.count).map { $0 == initialValue.rawValue }
        self.isFlipped = initialValue == .signup
        self.setFeedBack()
    }

    var isRecovering: Bool {
        formType == .recovering
    }

    var isLogging: Bool {
        formType == .login
    }

    var isUsingID: Bool {
        formType == .biometric
    }

    var formType: FormType {
        didSet {
            (0..<viewFlags.count).forEach {
                viewFlags[$0] = $0 == formType.rawValue
            }
            guard canFlip else { return }
            self.isFlipped.toggle()
            FeedbackGenerator.generator.impactOccurred()
        }
    }

    func setIdType() {
        self.canFlip = false
        self.formType = Preferences.biometrics && Preferences.previouslyLoggedID ? .biometric : .login
        self.canFlip = true
    }

    var actionName: String {
        formViewModels[formType.rawValue].actionName
    }

    var rotationDegrees: Double {
        isUsingID ? 0 : isFlipped ? 180 : 0
    }

    var formViewModels: [FormViewModel] = [
        FormViewModel(actionName: "Login", inputViewModels: [
            InputFieldViewModel(fieldType: .textField, placeholder: "User"),
            InputFieldViewModel(fieldType: .secureField, placeholder: "Password")
        ]),
        FormViewModel(actionName: "Sign Up", inputViewModels: [
            InputFieldViewModel(fieldType: .textField, placeholder: "User"),
            InputFieldViewModel(fieldType: .secureField, placeholder: "Password"),
            InputFieldViewModel(fieldType: .secureField, placeholder: "Confirm Password")
        ]),
        FormViewModel(actionName: "Recover Password", inputViewModels: [
            InputFieldViewModel(fieldType: .secureField, placeholder: "New Password"),
            InputFieldViewModel(fieldType: .secureField, placeholder: "Confirm New Password")
        ]),
        FormViewModel(actionName: "Login", inputViewModels: [])
    ]

    func validateAction() {
        let formViewModels = formViewModels[formType.rawValue]
        switch formType {
        case .login:
            let name: String = formViewModels.inputViewModels.first?.text.text ?? ""
            let pass: String = formViewModels.inputViewModels.last?.text.text ?? ""
            let condition = name != "" && pass != ""
            if condition {
                self.compareUserData(name: name, pass: pass)
            } else {
                let texts: [String] = [name, pass]
                zip(texts.indices, texts).forEach {
                    formViewModels.inputViewModels[$0].text.showFeedback = $1.isEmpty
                }
            }
        case .signup:
            let name: String = formViewModels.inputViewModels.first?.text.text ?? ""
            let pass0: String = formViewModels.inputViewModels[1].text.text
            let pass1: String = formViewModels.inputViewModels.last?.text.text ?? ""
            let condition = name != "" && pass0 != "" && pass1 != "" && pass0 == pass1
            if condition {
                self.setUserData(user: name, name: name, pass: pass0)
            } else {
                let texts: [String] = [name, pass0, pass1]
                zip(texts.indices, texts).forEach {
                    formViewModels.inputViewModels[$0].text.showFeedback = $1 == ""
                }
            }
        case .recovering:
            let pass0: String = formViewModels.inputViewModels.first?.text.text ?? ""
            let pass1: String = formViewModels.inputViewModels.last?.text.text ?? ""
            let condition: Bool = pass0 != "" && pass1 != "" && pass0 == pass1
            if condition {
                self.updatePassword(value: pass0)
            } else {
                let texts: [String] = [pass0, pass1]
                zip(texts.indices, texts).forEach {
                    formViewModels.inputViewModels[$0].text.showFeedback = $1 == ""
                }
            }
        case .biometric:
            self.resetTextFields()
            self.setIdType()
            self.setSession()
        }
    }

    private func compareUserData(name: String, pass: String) {
        self.useCase.userData.sink(receiveCompletion: logDb) { [weak self] in
            if let userData = $0,
               userData.name?.lowercased() == name.lowercased(),
               userData.pass == pass {
                Preferences.previouslyLogged = true
                if Preferences.biometrics {
                    Preferences.previouslyLoggedID = true
                }
                self?.resetTextFields()
                self?.setIdType()
                self?.setSession()
            } else {
                debugPrint("[📱]: MainViewModel.compareUserData failure authentication wrong password or user")
            }
        }
        .store(in: &subscriptions)
    }

    private func setUserData(user: String, name: String, pass: String) {
        self.useCase.userData.sink { [weak self] in
            if case .failure(let error) = $0,
                DataSource.emptyError as NSError == error as NSError {
                debugPrint("[📱]: error \(error)")
                self?.createAndSaveUserData(user: user, name: name, pass: pass)
                self?.resetTextFields()
                self?.formType = .login
            }
        } receiveValue: { [weak self] in
            if let userData = $0 {
                userData.name = name
                userData.pass = pass
                DataSource.shared.saveContext()
                self?.resetTextFields()
                self?.formType = .login
            }
        }
        .store(in: &subscriptions)
    }

    private func updatePassword(value: String) {
        self.useCase.userData.sink(receiveCompletion: logDb) { [weak self] in
            if let userData = $0 {
                userData.pass = value
                DataSource.shared.saveContext()
                self?.resetTextFields()
                self?.formType = .login
            }
        }
        .store(in: &subscriptions)
    }

    private func createAndSaveUserData(user: String, name: String, pass: String) {
        let biometrics: Bool = UserDefaults.standard.bool(forKey: Constant.BIOMETRICS)
        let sesion = Sesion(isLogged: true, loginDate: Date(), biometrics: biometrics)
        let _ = User(name: name, pass: pass, firstLoginDidPass: true, sesion: sesion)
        DataSource.shared.saveContext()
    }

    func authenticateWithBiometrics() {
        let context = LAContext()
        var error: NSError?
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            let reason = "authentication required"
            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) { success, authenticationError in
                DispatchQueue.main.async {
                    if success {
                        self.validateAction()
                    } else if let authenticationError {
                        debugPrint(authenticationError)
                    }
                }
            }
        } else {
            debugPrint("failure authentication")
        }
    }

    private func resetTextFields() {
        formViewModels.forEach {
            $0.inputViewModels.forEach {
                $0.text.text = ""
            }
        }
    }

    private func setFeedBack() {
        (0 ..< formViewModels.count).forEach {
            guard let formType = FormType(rawValue: $0) else { return }
            formViewModels[$0].inputViewModels.forEach { input in
                input.text._showFeedBack = {
                    let text: String = input.text.text
                    switch formType {
                    case .login, .biometric: return false
                    case .signup: return false
                    case .recovering: return !text.isEmpty && text.count < 4
                    }
                }
            }
        }
    }

    func checkSesion() {
        self.useCase.userData.sink(receiveCompletion: logDb) { [weak self] in
            if let userData = $0 {
                let session = userData.sesion
                let isLogged = session?.isLogged ?? false
                let loginDate = session?.loginDate ?? Date(timeIntervalSince1970: 0)
                let condition: Bool = isLogged && (loginDate.timeIntervalSince1970 > Date.now.timeIntervalSince1970 - 2*3600*24)
                self?.isLogged = condition
                if !condition {
                    userData.sesion?.isLogged = false
                    userData.sesion?.loginDate = nil
                    DataSource.shared.saveContext()
                }
            }
        }
        .store(in: &subscriptions)
    }

    private func setSession() {
        self.useCase.userData.sink(receiveCompletion: logDb) { [weak self] in
            if let userData = $0 {
                userData.sesion?.isLogged = true
                userData.sesion?.loginDate = Date()
                DataSource.shared.saveContext()
                self?.isLogged = true
            }
        }
        .store(in: &subscriptions)
    }

    private lazy var logDb: (Subscribers.Completion<Error>) -> Void = { completion in
        if case .failure(let error) = completion,
            DataSource.emptyError as NSError == error as NSError {
            debugPrint("[📱]: error \(error)")
        }
    }

}
