//
//  MainUseCase.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 08/02/24.
//

import Foundation
import Combine

class MainUseCase {

    private let repository: MainRepository

    init(repository: MainRepository) {
        self.repository = repository
    }

    var userData: AnyPublisher<User?, Error> {
        return repository.userData
            .tryMap {
                $0.first
            }
            .eraseToAnyPublisher()
    }

}
