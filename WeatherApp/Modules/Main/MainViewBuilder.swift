//
//  MainViewBuilder.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 08/02/24.
//

import SwiftUI

struct MainViewBuilder {

    @ViewBuilder
    func build() -> some View {
        let repository = MainRepository(dbClient: .shared)
        let useCase = MainUseCase(repository: repository)
        let viewModel = MainViewModel(useCase: useCase)
        MainView(viewModel: viewModel)
    }

}
