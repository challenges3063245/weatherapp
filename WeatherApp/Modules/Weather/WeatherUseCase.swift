//
//  WeatherUseCase.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 06/02/24.
//

import Foundation
import Combine

class WeatherUseCase {

    let repository: WeatherRepository
    let locationRepository: LocationRepository

    init(repository: WeatherRepository, locationRepository: LocationRepository) {
        self.repository = repository
        self.locationRepository = locationRepository
    }

    func getWeatherData(latitude: Double, longitude: Double) -> AnyPublisher<WeatherData, Error> {
        return repository.getWeatherData(latitude: latitude, longitude: longitude)
            .eraseToAnyPublisher()
    }

}
