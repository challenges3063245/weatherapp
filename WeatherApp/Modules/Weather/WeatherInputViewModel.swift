//
//  WeatherInputViewModel.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 06/02/24.
//

import Foundation
import Combine

struct WeatherInputViewModel {
    let onAppearPublisher = PassthroughSubject<Void, Never>()
    let refreshPublisher = PassthroughSubject<Void, Never>()
}
