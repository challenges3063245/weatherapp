//
//  WeatherBuilder.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 06/02/24.
//

import Foundation
import SwiftUI

struct WeatherBuilder {

    @ViewBuilder
    func build() -> some View {
        let repository = WeatherRepository()
        let locationRepository: LocationRepository = .shared
        let useCase = WeatherUseCase(repository: repository, locationRepository: locationRepository)
        let viewModel = WeatherViewModel(useCase: useCase)
        let input = WeatherInputViewModel()
        WeatherView(viewModel: viewModel, input: input)
    }

}
