//
//  Grid.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 06/02/24.
//

import SwiftUI
import CoreData

struct GridView<T: WeatherData, U: Identifiable, Content: View>: View {

    @Environment(\.verticalSizeClass) var verticalSizeClass
    @Environment(\.horizontalSizeClass) var horizontalSizeClass

    let content: (U) -> Content

    @FetchRequest private var objects: FetchedResults<T>

    private var itemsViewData: [ItemViewData] {
        guard let object = objects.first else { return [] }
        let day = object.forecast?.forecastDays.first?.day
        let tempC: String = "\(object.current?.tempC ?? 0)"
        let windkph: String = "\(object.current?.windkph ?? 0)"
        let humidity: String = "\(object.current?.humidity ?? 0)"
        let maxTemp: String = "\(day?.maxTempC ?? 0) °C"
        let minTemp: String = "\(day?.minTempC ?? 0) °C"
        return [
            ItemViewData(title: "Weather", subTitle: tempC + " °C", imageName: "sun.haze", url: object.current?.condition?.icon ?? ""),
            ItemViewData(title: "Degree", subTitle: humidity + "%", imageName: "drop.degreesign"),
            ItemViewData(title: "Min " + minTemp, subTitle: "Max " + maxTemp, imageName: "thermometer.medium"),
            ItemViewData(title: "Wind", subTitle: windkph + " Km/hr", imageName: "wind")
        ]
    }

    private var locationViewData: [LocationViewData] {
        guard let object = objects.first else { return [] }
        let forecast = object.forecast?.forecastDays.first
        let name = object.location?.name ?? ""
        let country = object.location?.country ?? ""
        return [
            LocationViewData(
                country: country, name: name,
                sunRise: forecast?.astro?.sunrise,
                sunSet: forecast?.astro?.sunset,
                moonRise: forecast?.astro?.moonrise,
                moonSet: forecast?.astro?.moonset,
                isMoonUp: forecast?.astro?.isMoonUp ?? 0,
                isSunUp: forecast?.astro?.isSunUp ?? 0
            )
        ]
    }

    private var columns: [GridItem] {
        horizontalSizeClass == .regular && verticalSizeClass == .regular
        ? Array(repeating: GridItem(.flexible()), count: 4)
        : Array(repeating: GridItem(.flexible()), count: 2)
    }

    init(@ViewBuilder content: @escaping (U) -> Content) {
        let fetchRequest: NSFetchRequest<T> = .init(entityName: String(describing: T.self))
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "date", ascending: false)]
        self._objects = FetchRequest<T>(fetchRequest: fetchRequest, animation: .smooth)
        self.content = content
    }

    var body: some View {
        VStack {
            LazyVGrid(columns: columns) {
                ForEach(itemsViewData) { item in
                    self.content(item as! U)
                }
            }
            if !locationViewData.isEmpty {
                LocationWeatherView(viewData: locationViewData.first!)
            }
        }
        .padding(.horizontal, 8)
    }

}
