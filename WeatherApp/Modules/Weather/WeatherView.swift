//
//  Weather.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 06/02/24.
//

import SwiftUI
import CoreData

struct WeatherView: View {

    private var viewModel: WeatherViewModel
    private let input: WeatherInputViewModel

    @FetchRequest private var objects: FetchedResults<User>

    init(viewModel: WeatherViewModel, input: WeatherInputViewModel) {
        self.viewModel = viewModel
        self.input = input
        let fetchRequest: NSFetchRequest<User> = .init(entityName: String(describing: User.self))
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "name", ascending: false)]
        self._objects = FetchRequest<User>(fetchRequest: fetchRequest, animation: .smooth)
        self.bind()
    }

    var user: UserViewData? {
        guard let user = objects.first else { return nil }
        return UserViewData(user: user)
    }

    var body: some View {
        ScrollView {
            VStack {
                if let user {
                    HStack {
                        Text(user.hi)
                            .font(.title3)
                            .bold()
                            .foregroundStyle(.gray)
                            .padding()
                        Spacer(minLength: 0)
                    }
                }
                GridView { (item: ItemViewData) in
                    ItemView(item: item)
                }
            }
        }
        .navigationBarTitle("Weather", displayMode: .large)
        .toolbarBackground(Color.accentColor, for: .navigationBar)
        .background {
            Color.customBackground
                .ignoresSafeArea()
        }
        .refreshable {
            input.refreshPublisher.send()
        }
    }

    private func bind() {
        viewModel.bind(input: input)
        self.input.onAppearPublisher.send()
    }

}
