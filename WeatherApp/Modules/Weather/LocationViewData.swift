//
//  LocationViewData.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 07/02/24.
//

import Foundation

struct LocationViewData: Identifiable {

    let id: UUID = .init()
    var country: String?
    var name: String

    var sunRise: String?
    var sunSet: String?
    var moonRise: String?
    var moonSet: String?
    var isMoonUp: Int16
    var isSunUp: Int16

    init(country: String? = nil, name: String, sunRise: String? = nil, sunSet: String? = nil, moonRise: String? = nil, moonSet: String? = nil, isMoonUp: Int16, isSunUp: Int16) {
        self.country = country
        self.name = name
        self.sunRise = sunRise
        self.sunSet = sunSet
        self.moonRise = moonRise
        self.moonSet = moonSet
        self.isMoonUp = isMoonUp
        self.isSunUp = isSunUp
    }

    var astroRise: String {
        isSunUp == 1 ? "Sunrise" : "Moonrise"
    }

    var astroRiseSub: String {
        isSunUp == 1 ? sunRise ?? "" : moonRise ?? ""
    }

    var astroRiseIcon: String {
        isSunUp == 1 ? "sunrise.fill" : "moonrise.fill"
    }

    var astroSet: String {
        isSunUp == 1 ? "Sunset" : "Moonset"
    }

    var astroSetSub: String {
        isSunUp == 1 ? sunSet ?? "" : moonSet ?? ""
    }

    var astroSetIcon: String {
        isSunUp == 1 ? "sunset.fill" : "moonset.fill"
    }

}
