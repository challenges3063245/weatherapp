//
//  LocationWeatherView.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 07/02/24.
//

import SwiftUI

struct LocationWeatherView: View {

    var viewData: LocationViewData

    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 15).fill(.background)
                .shadow(radius: 4)
            HStack {
                leftView
                    .frame(height: 160)
                rightView
                    .frame(height: 160)
            }
        }
    }

    @ViewBuilder
    private var leftView: some View {
        ZStack {
            VStack {
                HStack {
                    Image(systemName: "map.fill")
                        .resizable()
                        .scaledToFit()
                        .frame(width: 40, height: 40)
                    Spacer(minLength: 0)
                }
                Spacer(minLength: 0)
                singleView(title: viewData.astroRise, subTitle: viewData.astroRiseSub, iconName: viewData.astroRiseIcon)
            }
        }
        .padding(16)
    }

    @ViewBuilder
    private var rightView: some View {
        ZStack {
            VStack {
                HStack {
                    Spacer(minLength: 0)
                    VStack {
                        Text(viewData.name)
                            .font(.title3)
                            .bold()

                        Text(viewData.country ?? "")
                            .foregroundStyle(.gray)
                    }
                }
                Spacer(minLength: 0)
                singleView(title: viewData.astroSet, subTitle: viewData.astroSetSub, iconName: viewData.astroSetIcon)
            }
        }
        .padding(16)
    }

    @ViewBuilder
    private func singleView(title: String, subTitle: String, iconName: String? = nil) -> some View {
        HStack {
            Image(systemName: iconName ?? "sun.haze")
                .resizable()
                .scaledToFit()
                .frame(width: 40, height: 40)
            Spacer(minLength: 0)
            VStack {
                Text(title)
                    .font(.title3)
                    .bold()

                Text(subTitle)
                    .foregroundStyle(.gray)
            }
        }
    }

}
