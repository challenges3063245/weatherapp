//
//  WeatherRepository.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 06/02/24.
//

import Foundation
import Combine

class WeatherRepository {

    let client: NetworkAPI = .shared
    var apiKey: String { "efa8bb4a31244fe4834210417240602" }
//    var urlString: String { "http://api.weatherapi.com/v1/current.json?key=\(apiKey)&aqi=no" }
    var urlString: String { "https://api.weatherapi.com/v1/forecast.json?key=\(apiKey)" }

    func getWeatherData(latitude: Double, longitude: Double) -> AnyPublisher<WeatherData, Error> {
        let urlString = self.urlString + "&q=" + "\(latitude)" + "," + "\(longitude)"
        let url = URL(string: urlString)!
        let request = URLRequest(url: url)
        return client.getSerializedData(with: request)
            .eraseToAnyPublisher()
    }

}
