//
//  WeatherViewModel.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 06/02/24.
//

import Foundation
import Combine
import CoreLocation

class WeatherViewModel {

    let useCase: WeatherUseCase
    var subscriptions = Set<AnyCancellable>()
    var location: CLLocationCoordinate2D?
    var getFirstData: Bool = false

    init(useCase: WeatherUseCase) {
        self.useCase = useCase
    }

    func bind(input: WeatherInputViewModel) {

        input.refreshPublisher.sink(receiveValue: getWeatherDataHandler)
            .store(in: &subscriptions)

        self.useCase.locationRepository.$location.sink { [weak self] location2D in
            guard let self, let location2D else { return }
            self.location = location2D
            if !self.getFirstData {
                getFirstData = true
                self.getWeatherDataHandler()
            }
        }
        .store(in: &subscriptions)
    }

    private lazy var getWeatherDataHandler: () -> Void = { [weak self] in
        guard let self else { return }
        let latitude = location?.latitude ?? 0
        let longitude = location?.longitude ?? 0
        self.useCase.getWeatherData(latitude: latitude, longitude: longitude).sink { completion in
            if case .failure(let error) = completion {
                debugPrint(error)
            }
        } receiveValue: {
            CoreDataManager.shared.saveContext()
            debugPrint($0)
        }
        .store(in: &self.subscriptions)
    }

}
