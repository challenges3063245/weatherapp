//
//  CameraView.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 08/02/24.
//

import SwiftUI

struct CameraView: UIViewControllerRepresentable {

    @Environment(\.presentationMode) var isPresented

    let onComplete: (Data) -> Void

    func makeUIViewController(context: Context) -> UIImagePickerController {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .camera
        imagePicker.allowsEditing = true
        imagePicker.delegate = context.coordinator
        return imagePicker
    }

    func updateUIViewController(_ uiViewController: UIImagePickerController, context: Context) { }

    func makeCoordinator() -> Coordinator {
        return Coordinator(picker: self)
    }

}

class Coordinator: NSObject, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    var picker: CameraView

    init(picker: CameraView) {
        self.picker = picker
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let selectedImage = info[.originalImage] as? UIImage else { return }
        if let data = selectedImage.pngData() {
            self.picker.onComplete(data)
        }
        self.picker.isPresented.wrappedValue.dismiss()
    }

}

