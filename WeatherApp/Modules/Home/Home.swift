//
//  Home.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 06/02/24.
//

import SwiftUI
import Combine

typealias Completion = () -> Void

public struct Home: View {

    @Binding var isLogged: Bool

    @State var weatherBuilder: WeatherBuilder = WeatherBuilder()
    @State var profileBuilder: ProfileBuilder = ProfileBuilder()

    init(isLogged: Binding<Bool>) {
        self._isLogged = isLogged
    }

    private var items: [TabItem] {
        [
            .init(weatherBuilder.build(), imageName: "thermometer.sun", name: "Weather"),
            .init(profileBuilder.build(isLogged: $isLogged), imageName: "person.fill", name: "Profile")
        ]
    }

    public var body: some View {
        TabView {
            ForEach(items) { item in
                NavigationStack {
                    item.view
                }.tabItem {
                    Image(systemName: item.imageName)
                    Text(item.name)
                }
            }
        }
        .onAppear {
            let tabBarAppearance = UITabBarAppearance()
            tabBarAppearance.configureWithOpaqueBackground()
            UITabBar.appearance().scrollEdgeAppearance = tabBarAppearance
        }
    }

}
