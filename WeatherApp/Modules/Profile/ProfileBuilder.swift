//
//  ProfileBuilder.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 07/02/24.
//

import SwiftUI
import Combine

struct ProfileBuilder {

    @ViewBuilder
    func build(isLogged: Binding<Bool>) -> some View {
        Profile(isLogged: isLogged)
    }

}
