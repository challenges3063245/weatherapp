//
//  Profile.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 06/02/24.
//

import SwiftUI
import CoreData
import PhotosUI

struct Profile: View {

    @State private var isEditing: Bool = false
    @State private var showPhotoPicker = false
    @State private var showCameraPicker = false
    @State private var selectedImage: Image? = nil
    @State private var selectedItem: PhotosPickerItem?

    @State private var name: String = ""
    @State private var email: String = ""
    @State private var phone: String = ""

    @Binding var isLogged: Bool

    @FetchRequest private var objects: FetchedResults<User>

    init(isLogged: Binding<Bool>) {
        self._isLogged = isLogged
        self._objects = FetchRequest<User>(fetchRequest: User.fetchRequest(), animation: .smooth)
    }

    var user: UserViewData? {
        guard let user = objects.first else { return nil }
        return UserViewData(user: user)
    }

    var image: Image? {
        guard  let data = objects.first?.imageData, let uiImage = UIImage(data: data) else {
            return nil
        }
        return Image(uiImage: uiImage)
    }

    var body: some View {
        ZStack {
            Color.customBackground
                .ignoresSafeArea()
            ScrollView {
                VStack {
                    ParallaxView(image: $selectedImage)

                    if let user {
                        Group {
                            EditableInput(isEditing: $isEditing, text: $name, placeholder: "Name", description: user.welcome)
                                .font(.title2)
                                .bold()

                            EditableInput(isEditing: $isEditing, text: $email, placeholder: "Email")
                                .foregroundStyle(.gray)
                                .font(.title3)

                            EditableInput(isEditing: $isEditing, text: $phone, placeholder: "Phone")
                                .foregroundStyle(.gray)
                                .font(.title3)
                        }
                        .padding()
                    }

                    Group {
                        Button {
                            withAnimation {
                                isEditing.toggle()
                                saveUserData()
                            }
                        } label: {
                            Text(isEditing ? "Save" : "Update data")
                                .foregroundStyle(.white)
                                .padding(10)
                                .frame(width: 200)
                                .background {
                                    RoundedRectangle(cornerRadius: 10)
                                        .fill(Color.accentColor)
                                }
                        }

                        Button {
                            withAnimation {
                                if !isEditing {
                                    self.logOut()
                                } else {
                                    isEditing = false
                                    setUserData()
                                }
                            }
                        } label: {
                            Text(isEditing ? "Cancel" : "Log out")
                                .foregroundStyle(Color.accentColor)
                                .padding(10)
                                .frame(width: 200)
                                .background {
                                    RoundedRectangle(cornerRadius: 10)
                                        .stroke(Color.accentColor)
                                }
                        }
                    }
                    .padding()
                }
            }
            .scrollDismissesKeyboard(.interactively)
            .overlay(alignment: .topTrailing) {
                photoButton
            }
            .onAppear {
                self.selectedImage = image
                self.setUserData()
            }
        }
    }

    @ViewBuilder
    private var photoButton: some View {
        Menu {
            Button {
                showCameraPicker.toggle()
            } label: {
                Label("Camera", systemImage: "camera")
            }

            Button {
                showPhotoPicker.toggle()
            } label: {
                Label("Galery", systemImage: "photo.on.rectangle.angled")
            }
        } label: {
            Image(systemName: "photo.badge.plus")
                .padding(10)
                .background(.black.opacity(0.7), in: .circle)
                .contentShape(.circle)
                .foregroundStyle(.white)
                .padding(.trailing)
        }
        .fullScreenCover(isPresented: $showPhotoPicker) {
            photoPicker
        }
        .fullScreenCover(isPresented: $showCameraPicker) {
            cameraPicker
        }
    }

    @ViewBuilder
    private var cameraPicker: some View {
        ZStack {
            Color.black
                .ignoresSafeArea()
            CameraView() { data in
                objects.first?.imageData = data
                DataSource.shared.saveContext()
                if let img = UIImage(data: data) {
                    selectedImage = Image(uiImage: img)
                }
            }
        }
    }

    @ViewBuilder
    private var photoPicker: some View {
        PhotoPicker(filter: .images) { results in
            PhotoPicker.convertToUIImageArray(fromResults: results) { imagesOrNil, errorOrNil in
                if let error = errorOrNil {
                    debugPrint(error)
                }
                if let images = imagesOrNil {
                    if let first = images.first {

                        let data = first.pngData()
                        objects.first?.imageData = data
                        DataSource.shared.saveContext()
                        selectedImage = Image(uiImage: first)
                    }
                }
            }
        }
        .edgesIgnoringSafeArea(.all)
    }

    @ViewBuilder
    private var cameraView: some View {
        PhotosPicker("Select an image", selection: $selectedItem, matching: .images)
            .edgesIgnoringSafeArea(.all)
            .onChange(of: selectedItem) {
                Task {
                    if let data = try? await selectedItem?.loadTransferable(type: Data.self), let img = UIImage(data: data) {
                        selectedImage = Image(uiImage: img)
                    }
                }
            }
    }

    private func logOut() {
        self.isLogged = false
        let sesion = self.objects.first?.sesion
        sesion?.isLogged = false
        sesion?.loginDate = nil
        DataSource.shared.saveContext()
    }

    private func saveUserData() {
        if !isEditing {
            objects.first?.name = name
            objects.first?.email = email
            objects.first?.phone = phone
            DataSource.shared.saveContext()
        }
    }

    private func setUserData() {
        if let user {
            self.name = user.name
            self.email = user.email
            self.phone = user.phone
        }
    }

}
