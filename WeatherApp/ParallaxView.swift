//
//  ParallaxView.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 06/02/24.
//

import SwiftUI

struct ParallaxView: View {

    @State var parallaxHeight: CGFloat = 250
    @Binding var image: Image?

    var body: some View {
        GeometryReader { geometry in
            let offsetY: CGFloat = geometry.frame(in: .global).origin.y
            let isScrolled: Bool = offsetY > 0
            Spacer()
                .frame(height: isScrolled ? parallaxHeight + offsetY : parallaxHeight)
                .background {
                    ZStack {
                        Color.accentColor
                            .offset(y: isScrolled ? -1.1*offsetY : 0)
                        VStack {
                            Group {
                                if let image {
                                    image
                                        .resizable()
                                } else {
                                    Image(systemName: "person.fill")
                                        .resizable()
                                }
                            }
                            .scaledToFill()
                            .padding(.top)
                            .offset(y: isScrolled ? -0.7*offsetY : 0)
                            .frame(maxWidth: 0.8*parallaxHeight, maxHeight: 0.8*parallaxHeight)
                            .clipShape(Circle())
                            Spacer(minLength: 0)
                        }
                    }
                }
        }
        .frame(height: parallaxHeight)
    }

}
