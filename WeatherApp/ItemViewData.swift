//
//  ItemViewData.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 06/02/24.
//

import Foundation

struct ItemViewData: Identifiable {

    let id: UUID = .init()
    var title: String
    var subTitle: String
    var imageName: String?
    var url: String?

    init(title: String, subTitle: String, imageName: String? = nil, url: String? = nil) {
        self.title = title
        self.subTitle = subTitle
        self.imageName = imageName
        self.url = url
    }

}
