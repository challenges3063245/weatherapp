//
//  EditableInput.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 08/02/24.
//

import SwiftUI

struct EditableInput: View {

    @Binding var isEditing: Bool
    @Binding var text: String

    var placeholder: String
    var description: String?

    var body: some View {
        ZStack {
            if isEditing {
                TextField(text: $text) {
                    Text(placeholder)
                }
                .multilineTextAlignment(.center)
            } else {
                Text(description ?? text)
            }
        }
        .overlay(alignment: .bottom) {
            Color.accentColor
                .frame(height: isEditing ? 1 : 0)
        }
    }

}
