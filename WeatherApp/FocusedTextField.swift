//
//  FocusedTextField.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 10/02/24.
//

import SwiftUI

struct FocusedTextField: View {

    @FocusState private var isFocused: Bool
    @Binding var text: String
    var placeholder: String
    var fieldType: FieldType

    let onfocusChange: (Bool) -> Void

    var body: some View {
        Group {
            if fieldType == .secureField {
                SecureField(text: $text) {
                    Text(placeholder)
                }
            } else {
                TextField(text: $text) {
                    Text(placeholder)
                }
            }
        }
        .focused($isFocused)
        .onChange(of: isFocused, setFocused)
    }

    func setFocused() {
        onfocusChange(isFocused)
    }

}
