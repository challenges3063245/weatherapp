//
//  WeatherAppApp.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 06/02/24.
//

import SwiftUI

@main
struct WeatherAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, CoreDataManager.shared.context)
        }
    }
}
