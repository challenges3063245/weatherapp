//
//  ContentView.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 06/02/24.
//

import SwiftUI

struct ContentView: View {

    @State private var mainViewBuider: MainViewBuilder = MainViewBuilder()

    var body: some View {
        mainViewBuider.build()
    }
}

#Preview {
    ContentView()
}
