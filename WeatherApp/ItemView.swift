//
//  ItemView.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 06/02/24.
//

import SwiftUI

struct ItemView: View {

    @State  var item: ItemViewData

    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 15)
                .fill(.background)
                .shadow(radius: 4)

            HStack {
                ZStack {
                    Image(systemName: item.imageName ?? "")
                        .resizable()
                        .frame(width: 35, height: 40)
                        .scaledToFit()
                        .padding(10)
                
                }
                VStack {
                    Text(item.title)
                        .font(.title3)
                        .bold()
                        .minimumScaleFactor(0.5)
                        .lineLimit(1)
                        .padding()

                    Text(item.subTitle)
                        .foregroundStyle(.gray)
                        .minimumScaleFactor(0.5)
                        .lineLimit(1)
                        .padding()
                }
            }
        }
    }
}
