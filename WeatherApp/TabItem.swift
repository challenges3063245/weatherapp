//
//  TabItem.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 06/02/24.
//

import SwiftUI

struct TabItem: Identifiable {
    var id: UUID = UUID()
    var view: AnyView
    var imageName: String
    var name: String

    init(id: UUID = UUID(), _ view: any View, imageName: String, name: String) {
        self.id = id
        self.view = AnyView(view)
        self.imageName = imageName
        self.name = name
    }

}
