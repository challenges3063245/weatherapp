//
//  LocationManager.swift
//  WeatherApp
//
//  Created by Ariel Díaz on 07/02/24.
//

import CoreLocation

class LocationRepository: NSObject, ObservableObject, CLLocationManagerDelegate {

    static let shared: LocationRepository = .init()
    private let manager = CLLocationManager()

    @Published var location: CLLocationCoordinate2D?

    override private init() {
        super.init()
        manager.delegate = self
        manager.requestLocation()
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        location = locations.first?.coordinate
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        debugPrint(error)
    }

}
